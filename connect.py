import paramiko
from socket import error as las


def con(adres='', user='root', passww='', job=''):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(adres, username=user, password=passww,
                    timeout=5)
        sin, sout, serr = ssh.exec_command(job)
        print(adres + ": Connect OK")
        return(adres, str(sout.readlines()))
    except las:
        print('ups timeout: ' + adres)
        return('timeout: ',  adres)
